// Test 004
// Source: 
//
// (let ((x 1) (y 2) (z 3)) (begin (if (> x y) (if (< z (* x y)) (* x (* y z)) (- z y)) (while (> z 0) (set! z (- z 1)))) 42))
// 
// Result (in RAM0): 42
// Generated on 20130309 at 20:3.
// -----------------------------------//
@1
D=A
@x
M=D
@2
D=A
@y
M=D
@3
D=A
@z
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done44.
@x
D=M
// Move to lhs40
@lhs40
M=D
// Leave RHS in D.
@y
D=M
@rhs41
M=D
// Subtract LHS from RHS
@lhs40
D=M
@rhs41
// These instructions are prescient ... 20130308
A=M
D=A-D
@one43
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done44
0;JMP
(one43)
@1
D=A
// Done with boolean op.
(bool-done44)
// If the result was zero, we jump to iffalse38
@iffalse38
D;JEQ
// Do test, leave result in D
// Leave LHS in D; ends at bool-done52.
@z
D=M
// Move to lhs48
@lhs48
M=D
// Leave RHS in D.
@x
D=M
@mullhs55
M=D
@y
D=M
@mulrhs56
M=D
// Set multctr53.
@mulrhs56
D=M
@multctr53
M=D
// Set mulsum54.
@0
D=A
@mulsum54
M=D
// Top of whiletop57
(whiletop57)
// Test; jump to whileend58 if zero.
// Leave LHS in D; ends at bool-done63.
@multctr53
D=M
// Move to lhs59
@lhs59
M=D
// Leave RHS in D.
@0
D=A
@rhs60
M=D
// Subtract LHS from RHS
@lhs59
D=M
@rhs60
// These instructions are prescient ... 20130308
A=M
D=A-D
@one62
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done63
0;JMP
(one62)
@1
D=A
// Done with boolean op.
(bool-done63)
@whileend58
D;JEQ
// Run body of whiletop57.
// Set mulsum54.
@mulsum54
D=M
@lhs64
M=D
@mullhs55
D=M
@rhs65
M=D
// Stick LHS result lhs64 in D
@lhs64
D=M
// Stick RHS result rhs65 in A
@rhs65
A=M
D=D+A
@mulsum54
M=D
// Set multctr53.
@multctr53
D=M
@lhs66
M=D
@1
D=A
@rhs67
M=D
// Stick LHS result lhs66 in D
@lhs66
D=M
// Stick RHS result rhs67 in A
@rhs67
A=M
D=D-A
@multctr53
M=D
// Go to top of loop.
@whiletop57
0;JMP
// End of whiletop57.
(whileend58)
@mulsum54
D=M
@rhs49
M=D
// Subtract LHS from RHS
@lhs48
D=M
@rhs49
// These instructions are prescient ... 20130308
A=M
D=A-D
@one51
// Jump and leave 1 in D if true.
D;JGT
@0
D=A
@bool-done52
0;JMP
(one51)
@1
D=A
// Done with boolean op.
(bool-done52)
// If the result was zero, we jump to iffalse46
@iffalse46
D;JEQ
@x
D=M
@mullhs70
M=D
@y
D=M
@mullhs74
M=D
@z
D=M
@mulrhs75
M=D
// Set multctr72.
@mulrhs75
D=M
@multctr72
M=D
// Set mulsum73.
@0
D=A
@mulsum73
M=D
// Top of whiletop76
(whiletop76)
// Test; jump to whileend77 if zero.
// Leave LHS in D; ends at bool-done82.
@multctr72
D=M
// Move to lhs78
@lhs78
M=D
// Leave RHS in D.
@0
D=A
@rhs79
M=D
// Subtract LHS from RHS
@lhs78
D=M
@rhs79
// These instructions are prescient ... 20130308
A=M
D=A-D
@one81
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done82
0;JMP
(one81)
@1
D=A
// Done with boolean op.
(bool-done82)
@whileend77
D;JEQ
// Run body of whiletop76.
// Set mulsum73.
@mulsum73
D=M
@lhs83
M=D
@mullhs74
D=M
@rhs84
M=D
// Stick LHS result lhs83 in D
@lhs83
D=M
// Stick RHS result rhs84 in A
@rhs84
A=M
D=D+A
@mulsum73
M=D
// Set multctr72.
@multctr72
D=M
@lhs85
M=D
@1
D=A
@rhs86
M=D
// Stick LHS result lhs85 in D
@lhs85
D=M
// Stick RHS result rhs86 in A
@rhs86
A=M
D=D-A
@multctr72
M=D
// Go to top of loop.
@whiletop76
0;JMP
// End of whiletop76.
(whileend77)
@mulsum73
D=M
@mulrhs71
M=D
// Set multctr68.
@mulrhs71
D=M
@multctr68
M=D
// Set mulsum69.
@0
D=A
@mulsum69
M=D
// Top of whiletop87
(whiletop87)
// Test; jump to whileend88 if zero.
// Leave LHS in D; ends at bool-done93.
@multctr68
D=M
// Move to lhs89
@lhs89
M=D
// Leave RHS in D.
@0
D=A
@rhs90
M=D
// Subtract LHS from RHS
@lhs89
D=M
@rhs90
// These instructions are prescient ... 20130308
A=M
D=A-D
@one92
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done93
0;JMP
(one92)
@1
D=A
// Done with boolean op.
(bool-done93)
@whileend88
D;JEQ
// Run body of whiletop87.
// Set mulsum69.
@mulsum69
D=M
@lhs94
M=D
@mullhs70
D=M
@rhs95
M=D
// Stick LHS result lhs94 in D
@lhs94
D=M
// Stick RHS result rhs95 in A
@rhs95
A=M
D=D+A
@mulsum69
M=D
// Set multctr68.
@multctr68
D=M
@lhs96
M=D
@1
D=A
@rhs97
M=D
// Stick LHS result lhs96 in D
@lhs96
D=M
// Stick RHS result rhs97 in A
@rhs97
A=M
D=D-A
@multctr68
M=D
// Go to top of loop.
@whiletop87
0;JMP
// End of whiletop87.
(whileend88)
@mulsum69
D=M
// Jump to the ifend47 when done with the true case.
@ifend47
0;JMP
(iffalse46)
@z
D=M
@lhs98
M=D
@y
D=M
@rhs99
M=D
// Stick LHS result lhs98 in D
@lhs98
D=M
// Stick RHS result rhs99 in A
@rhs99
A=M
D=D-A
(ifend47)
// Jump to the ifend39 when done with the true case.
@ifend39
0;JMP
(iffalse38)
// Top of whiletop100
(whiletop100)
// Test; jump to whileend101 if zero.
// Leave LHS in D; ends at bool-done106.
@z
D=M
// Move to lhs102
@lhs102
M=D
// Leave RHS in D.
@0
D=A
@rhs103
M=D
// Subtract LHS from RHS
@lhs102
D=M
@rhs103
// These instructions are prescient ... 20130308
A=M
D=A-D
@one105
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done106
0;JMP
(one105)
@1
D=A
// Done with boolean op.
(bool-done106)
@whileend101
D;JEQ
// Run body of whiletop100.
// Set z.
@z
D=M
@lhs107
M=D
@1
D=A
@rhs108
M=D
// Stick LHS result lhs107 in D
@lhs107
D=M
// Stick RHS result rhs108 in A
@rhs108
A=M
D=D-A
@z
M=D
// Go to top of loop.
@whiletop100
0;JMP
// End of whiletop100.
(whileend101)
(ifend39)
@42
D=A
// Move D register to RAM[0] for final result.
@0
M=D
(ENDOFLINE)
@ENDOFLINE
0;JMP
