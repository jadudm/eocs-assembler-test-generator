// Test 001
// Source: 
//
// (let ((sum 0) (x 10)) (begin (while (> x 0) (begin (set! sum (+ x sum)) (set! x (- x 1)))) sum))
// 
// Result (in RAM0): 55
// Generated on 20130309 at 20:3.
// -----------------------------------//
@0
D=A
@sum
M=D
@10
D=A
@x
M=D
// Top of whiletop0
(whiletop0)
// Test; jump to whileend1 if zero.
// Leave LHS in D; ends at bool-done6.
@x
D=M
// Move to lhs2
@lhs2
M=D
// Leave RHS in D.
@0
D=A
@rhs3
M=D
// Subtract LHS from RHS
@lhs2
D=M
@rhs3
// These instructions are prescient ... 20130308
A=M
D=A-D
@one5
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done6
0;JMP
(one5)
@1
D=A
// Done with boolean op.
(bool-done6)
@whileend1
D;JEQ
// Run body of whiletop0.
// Set sum.
@x
D=M
@lhs7
M=D
@sum
D=M
@rhs8
M=D
// Stick LHS result lhs7 in D
@lhs7
D=M
// Stick RHS result rhs8 in A
@rhs8
A=M
D=D+A
@sum
M=D
// Set x.
@x
D=M
@lhs9
M=D
@1
D=A
@rhs10
M=D
// Stick LHS result lhs9 in D
@lhs9
D=M
// Stick RHS result rhs10 in A
@rhs10
A=M
D=D-A
@x
M=D
// Go to top of loop.
@whiletop0
0;JMP
// End of whiletop0.
(whileend1)
@sum
D=M
// Move D register to RAM[0] for final result.
@0
M=D
(ENDOFLINE)
@ENDOFLINE
0;JMP
