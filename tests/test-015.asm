// Test 015
// Source: 
//
// (- 1 7)
// 
// Result (in RAM0): -6
// Generated on 20130309 at 19:54.
// -----------------------------------//
@1
D=A
@lhs940
M=D
@7
D=A
@rhs941
M=D
// Stick LHS result lhs940 in D
@lhs940
D=M
// Stick RHS result rhs941 in A
@rhs941
A=M
D=D-A
// Move D register to RAM[0] for final result.
@0
M=D
(ENDOFLINE)
@ENDOFLINE
0;JMP
