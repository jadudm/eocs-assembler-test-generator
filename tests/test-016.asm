// Test 016
// Source: 
//
// (if (> (if (< (* 0 7) 9) (if (> (if (< (* (let ((x67 5)) (- x67 x67)) 0) (+ (+ 6 6) (- 3 2))) (let ((x68 5)) 9) 8) (let ((x69 4)) (+ (let ((x70 7)) (* 3 (if (> 8 3) 4 x69))) x69))) (- 9 (- (if (< 8 3) 1 1) (if (< 7 7) 5 4))) 9) (let ((x71 2)) x71)) (- (if (> 6 1) (+ 4 (- 2 0)) 9) (let ((x72 6)) (let ((x73 8)) (if (< (if (> x72 (+ 4 (+ 5 7))) (+ x73 (let ((x74 0)) x74)) (let ((x75 2)) (let ((x76 1)) (let ((x77 4)) 3)))) (+ (* (let ((x78 0)) (let ((x79 4)) x73)) (+ 1 x73)) (* (if (> x72 3) x73 7) 6))) x73 (let ((x80 0)) x73)))))) 8 6)
// 
// Result (in RAM0): 8
// Generated on 20130309 at 19:54.
// -----------------------------------//
// Do test, leave result in D
// Leave LHS in D; ends at bool-done949.
// Do test, leave result in D
// Leave LHS in D; ends at bool-done957.
@0
D=A
@mullhs960
M=D
@7
D=A
@mulrhs961
M=D
// Set multctr958.
@mulrhs961
D=M
@multctr958
M=D
// Set mulsum959.
@0
D=A
@mulsum959
M=D
// Top of whiletop962
(whiletop962)
// Test; jump to whileend963 if zero.
// Leave LHS in D; ends at bool-done968.
@multctr958
D=M
// Move to lhs964
@lhs964
M=D
// Leave RHS in D.
@0
D=A
@rhs965
M=D
// Subtract LHS from RHS
@lhs964
D=M
@rhs965
// These instructions are prescient ... 20130308
A=M
D=A-D
@one967
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done968
0;JMP
(one967)
@1
D=A
// Done with boolean op.
(bool-done968)
@whileend963
D;JEQ
// Run body of whiletop962.
// Set mulsum959.
@mulsum959
D=M
@lhs969
M=D
@mullhs960
D=M
@rhs970
M=D
// Stick LHS result lhs969 in D
@lhs969
D=M
// Stick RHS result rhs970 in A
@rhs970
A=M
D=D+A
@mulsum959
M=D
// Set multctr958.
@multctr958
D=M
@lhs971
M=D
@1
D=A
@rhs972
M=D
// Stick LHS result lhs971 in D
@lhs971
D=M
// Stick RHS result rhs972 in A
@rhs972
A=M
D=D-A
@multctr958
M=D
// Go to top of loop.
@whiletop962
0;JMP
// End of whiletop962.
(whileend963)
@mulsum959
D=M
// Move to lhs953
@lhs953
M=D
// Leave RHS in D.
@9
D=A
@rhs954
M=D
// Subtract LHS from RHS
@lhs953
D=M
@rhs954
// These instructions are prescient ... 20130308
A=M
D=A-D
@one956
// Jump and leave 1 in D if true.
D;JGT
@0
D=A
@bool-done957
0;JMP
(one956)
@1
D=A
// Done with boolean op.
(bool-done957)
// If the result was zero, we jump to iffalse951
@iffalse951
D;JEQ
// Do test, leave result in D
// Leave LHS in D; ends at bool-done980.
// Do test, leave result in D
// Leave LHS in D; ends at bool-done988.
@5
D=A
@x67
M=D
@x67
D=M
@lhs993
M=D
@x67
D=M
@rhs994
M=D
// Stick LHS result lhs993 in D
@lhs993
D=M
// Stick RHS result rhs994 in A
@rhs994
A=M
D=D-A
@mullhs991
M=D
@0
D=A
@mulrhs992
M=D
// Set multctr989.
@mulrhs992
D=M
@multctr989
M=D
// Set mulsum990.
@0
D=A
@mulsum990
M=D
// Top of whiletop995
(whiletop995)
// Test; jump to whileend996 if zero.
// Leave LHS in D; ends at bool-done1001.
@multctr989
D=M
// Move to lhs997
@lhs997
M=D
// Leave RHS in D.
@0
D=A
@rhs998
M=D
// Subtract LHS from RHS
@lhs997
D=M
@rhs998
// These instructions are prescient ... 20130308
A=M
D=A-D
@one1000
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done1001
0;JMP
(one1000)
@1
D=A
// Done with boolean op.
(bool-done1001)
@whileend996
D;JEQ
// Run body of whiletop995.
// Set mulsum990.
@mulsum990
D=M
@lhs1002
M=D
@mullhs991
D=M
@rhs1003
M=D
// Stick LHS result lhs1002 in D
@lhs1002
D=M
// Stick RHS result rhs1003 in A
@rhs1003
A=M
D=D+A
@mulsum990
M=D
// Set multctr989.
@multctr989
D=M
@lhs1004
M=D
@1
D=A
@rhs1005
M=D
// Stick LHS result lhs1004 in D
@lhs1004
D=M
// Stick RHS result rhs1005 in A
@rhs1005
A=M
D=D-A
@multctr989
M=D
// Go to top of loop.
@whiletop995
0;JMP
// End of whiletop995.
(whileend996)
@mulsum990
D=M
// Move to lhs984
@lhs984
M=D
// Leave RHS in D.
@6
D=A
@lhs1008
M=D
@6
D=A
@rhs1009
M=D
// Stick LHS result lhs1008 in D
@lhs1008
D=M
// Stick RHS result rhs1009 in A
@rhs1009
A=M
D=D+A
@lhs1006
M=D
@3
D=A
@lhs1010
M=D
@2
D=A
@rhs1011
M=D
// Stick LHS result lhs1010 in D
@lhs1010
D=M
// Stick RHS result rhs1011 in A
@rhs1011
A=M
D=D-A
@rhs1007
M=D
// Stick LHS result lhs1006 in D
@lhs1006
D=M
// Stick RHS result rhs1007 in A
@rhs1007
A=M
D=D+A
@rhs985
M=D
// Subtract LHS from RHS
@lhs984
D=M
@rhs985
// These instructions are prescient ... 20130308
A=M
D=A-D
@one987
// Jump and leave 1 in D if true.
D;JGT
@0
D=A
@bool-done988
0;JMP
(one987)
@1
D=A
// Done with boolean op.
(bool-done988)
// If the result was zero, we jump to iffalse982
@iffalse982
D;JEQ
@5
D=A
@x68
M=D
@9
D=A
// Jump to the ifend983 when done with the true case.
@ifend983
0;JMP
(iffalse982)
@8
D=A
(ifend983)
// Move to lhs976
@lhs976
M=D
// Leave RHS in D.
@4
D=A
@x69
M=D
@7
D=A
@x70
M=D
@3
D=A
@mullhs1016
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done1025.
@8
D=A
// Move to lhs1021
@lhs1021
M=D
// Leave RHS in D.
@3
D=A
@rhs1022
M=D
// Subtract LHS from RHS
@lhs1021
D=M
@rhs1022
// These instructions are prescient ... 20130308
A=M
D=A-D
@one1024
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done1025
0;JMP
(one1024)
@1
D=A
// Done with boolean op.
(bool-done1025)
// If the result was zero, we jump to iffalse1019
@iffalse1019
D;JEQ
@4
D=A
// Jump to the ifend1020 when done with the true case.
@ifend1020
0;JMP
(iffalse1019)
@x69
D=M
(ifend1020)
@mulrhs1017
M=D
// Set multctr1014.
@mulrhs1017
D=M
@multctr1014
M=D
// Set mulsum1015.
@0
D=A
@mulsum1015
M=D
// Top of whiletop1026
(whiletop1026)
// Test; jump to whileend1027 if zero.
// Leave LHS in D; ends at bool-done1032.
@multctr1014
D=M
// Move to lhs1028
@lhs1028
M=D
// Leave RHS in D.
@0
D=A
@rhs1029
M=D
// Subtract LHS from RHS
@lhs1028
D=M
@rhs1029
// These instructions are prescient ... 20130308
A=M
D=A-D
@one1031
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done1032
0;JMP
(one1031)
@1
D=A
// Done with boolean op.
(bool-done1032)
@whileend1027
D;JEQ
// Run body of whiletop1026.
// Set mulsum1015.
@mulsum1015
D=M
@lhs1033
M=D
@mullhs1016
D=M
@rhs1034
M=D
// Stick LHS result lhs1033 in D
@lhs1033
D=M
// Stick RHS result rhs1034 in A
@rhs1034
A=M
D=D+A
@mulsum1015
M=D
// Set multctr1014.
@multctr1014
D=M
@lhs1035
M=D
@1
D=A
@rhs1036
M=D
// Stick LHS result lhs1035 in D
@lhs1035
D=M
// Stick RHS result rhs1036 in A
@rhs1036
A=M
D=D-A
@multctr1014
M=D
// Go to top of loop.
@whiletop1026
0;JMP
// End of whiletop1026.
(whileend1027)
@mulsum1015
D=M
@lhs1012
M=D
@x69
D=M
@rhs1013
M=D
// Stick LHS result lhs1012 in D
@lhs1012
D=M
// Stick RHS result rhs1013 in A
@rhs1013
A=M
D=D+A
@rhs977
M=D
// Subtract LHS from RHS
@lhs976
D=M
@rhs977
// These instructions are prescient ... 20130308
A=M
D=A-D
@one979
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done980
0;JMP
(one979)
@1
D=A
// Done with boolean op.
(bool-done980)
// If the result was zero, we jump to iffalse974
@iffalse974
D;JEQ
@9
D=A
@lhs1037
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done1048.
@8
D=A
// Move to lhs1044
@lhs1044
M=D
// Leave RHS in D.
@3
D=A
@rhs1045
M=D
// Subtract LHS from RHS
@lhs1044
D=M
@rhs1045
// These instructions are prescient ... 20130308
A=M
D=A-D
@one1047
// Jump and leave 1 in D if true.
D;JGT
@0
D=A
@bool-done1048
0;JMP
(one1047)
@1
D=A
// Done with boolean op.
(bool-done1048)
// If the result was zero, we jump to iffalse1042
@iffalse1042
D;JEQ
@1
D=A
// Jump to the ifend1043 when done with the true case.
@ifend1043
0;JMP
(iffalse1042)
@1
D=A
(ifend1043)
@lhs1039
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done1056.
@7
D=A
// Move to lhs1052
@lhs1052
M=D
// Leave RHS in D.
@7
D=A
@rhs1053
M=D
// Subtract LHS from RHS
@lhs1052
D=M
@rhs1053
// These instructions are prescient ... 20130308
A=M
D=A-D
@one1055
// Jump and leave 1 in D if true.
D;JGT
@0
D=A
@bool-done1056
0;JMP
(one1055)
@1
D=A
// Done with boolean op.
(bool-done1056)
// If the result was zero, we jump to iffalse1050
@iffalse1050
D;JEQ
@5
D=A
// Jump to the ifend1051 when done with the true case.
@ifend1051
0;JMP
(iffalse1050)
@4
D=A
(ifend1051)
@rhs1040
M=D
// Stick LHS result lhs1039 in D
@lhs1039
D=M
// Stick RHS result rhs1040 in A
@rhs1040
A=M
D=D-A
@rhs1038
M=D
// Stick LHS result lhs1037 in D
@lhs1037
D=M
// Stick RHS result rhs1038 in A
@rhs1038
A=M
D=D-A
// Jump to the ifend975 when done with the true case.
@ifend975
0;JMP
(iffalse974)
@9
D=A
(ifend975)
// Jump to the ifend952 when done with the true case.
@ifend952
0;JMP
(iffalse951)
@2
D=A
@x71
M=D
@x71
D=M
(ifend952)
// Move to lhs945
@lhs945
M=D
// Leave RHS in D.
// Do test, leave result in D
// Leave LHS in D; ends at bool-done1066.
@6
D=A
// Move to lhs1062
@lhs1062
M=D
// Leave RHS in D.
@1
D=A
@rhs1063
M=D
// Subtract LHS from RHS
@lhs1062
D=M
@rhs1063
// These instructions are prescient ... 20130308
A=M
D=A-D
@one1065
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done1066
0;JMP
(one1065)
@1
D=A
// Done with boolean op.
(bool-done1066)
// If the result was zero, we jump to iffalse1060
@iffalse1060
D;JEQ
@4
D=A
@lhs1067
M=D
@2
D=A
@lhs1069
M=D
@0
D=A
@rhs1070
M=D
// Stick LHS result lhs1069 in D
@lhs1069
D=M
// Stick RHS result rhs1070 in A
@rhs1070
A=M
D=D-A
@rhs1068
M=D
// Stick LHS result lhs1067 in D
@lhs1067
D=M
// Stick RHS result rhs1068 in A
@rhs1068
A=M
D=D+A
// Jump to the ifend1061 when done with the true case.
@ifend1061
0;JMP
(iffalse1060)
@9
D=A
(ifend1061)
@lhs1057
M=D
@6
D=A
@x72
M=D
@8
D=A
@x73
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done1078.
// Do test, leave result in D
// Leave LHS in D; ends at bool-done1086.
@x72
D=M
// Move to lhs1082
@lhs1082
M=D
// Leave RHS in D.
@4
D=A
@lhs1087
M=D
@5
D=A
@lhs1089
M=D
@7
D=A
@rhs1090
M=D
// Stick LHS result lhs1089 in D
@lhs1089
D=M
// Stick RHS result rhs1090 in A
@rhs1090
A=M
D=D+A
@rhs1088
M=D
// Stick LHS result lhs1087 in D
@lhs1087
D=M
// Stick RHS result rhs1088 in A
@rhs1088
A=M
D=D+A
@rhs1083
M=D
// Subtract LHS from RHS
@lhs1082
D=M
@rhs1083
// These instructions are prescient ... 20130308
A=M
D=A-D
@one1085
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done1086
0;JMP
(one1085)
@1
D=A
// Done with boolean op.
(bool-done1086)
// If the result was zero, we jump to iffalse1080
@iffalse1080
D;JEQ
@x73
D=M
@lhs1091
M=D
@0
D=A
@x74
M=D
@x74
D=M
@rhs1092
M=D
// Stick LHS result lhs1091 in D
@lhs1091
D=M
// Stick RHS result rhs1092 in A
@rhs1092
A=M
D=D+A
// Jump to the ifend1081 when done with the true case.
@ifend1081
0;JMP
(iffalse1080)
@2
D=A
@x75
M=D
@1
D=A
@x76
M=D
@4
D=A
@x77
M=D
@3
D=A
(ifend1081)
// Move to lhs1074
@lhs1074
M=D
// Leave RHS in D.
@0
D=A
@x78
M=D
@4
D=A
@x79
M=D
@x73
D=M
@mullhs1097
M=D
@1
D=A
@lhs1099
M=D
@x73
D=M
@rhs1100
M=D
// Stick LHS result lhs1099 in D
@lhs1099
D=M
// Stick RHS result rhs1100 in A
@rhs1100
A=M
D=D+A
@mulrhs1098
M=D
// Set multctr1095.
@mulrhs1098
D=M
@multctr1095
M=D
// Set mulsum1096.
@0
D=A
@mulsum1096
M=D
// Top of whiletop1101
(whiletop1101)
// Test; jump to whileend1102 if zero.
// Leave LHS in D; ends at bool-done1107.
@multctr1095
D=M
// Move to lhs1103
@lhs1103
M=D
// Leave RHS in D.
@0
D=A
@rhs1104
M=D
// Subtract LHS from RHS
@lhs1103
D=M
@rhs1104
// These instructions are prescient ... 20130308
A=M
D=A-D
@one1106
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done1107
0;JMP
(one1106)
@1
D=A
// Done with boolean op.
(bool-done1107)
@whileend1102
D;JEQ
// Run body of whiletop1101.
// Set mulsum1096.
@mulsum1096
D=M
@lhs1108
M=D
@mullhs1097
D=M
@rhs1109
M=D
// Stick LHS result lhs1108 in D
@lhs1108
D=M
// Stick RHS result rhs1109 in A
@rhs1109
A=M
D=D+A
@mulsum1096
M=D
// Set multctr1095.
@multctr1095
D=M
@lhs1110
M=D
@1
D=A
@rhs1111
M=D
// Stick LHS result lhs1110 in D
@lhs1110
D=M
// Stick RHS result rhs1111 in A
@rhs1111
A=M
D=D-A
@multctr1095
M=D
// Go to top of loop.
@whiletop1101
0;JMP
// End of whiletop1101.
(whileend1102)
@mulsum1096
D=M
@lhs1093
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done1123.
@x72
D=M
// Move to lhs1119
@lhs1119
M=D
// Leave RHS in D.
@3
D=A
@rhs1120
M=D
// Subtract LHS from RHS
@lhs1119
D=M
@rhs1120
// These instructions are prescient ... 20130308
A=M
D=A-D
@one1122
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done1123
0;JMP
(one1122)
@1
D=A
// Done with boolean op.
(bool-done1123)
// If the result was zero, we jump to iffalse1117
@iffalse1117
D;JEQ
@x73
D=M
// Jump to the ifend1118 when done with the true case.
@ifend1118
0;JMP
(iffalse1117)
@7
D=A
(ifend1118)
@mullhs1114
M=D
@6
D=A
@mulrhs1115
M=D
// Set multctr1112.
@mulrhs1115
D=M
@multctr1112
M=D
// Set mulsum1113.
@0
D=A
@mulsum1113
M=D
// Top of whiletop1124
(whiletop1124)
// Test; jump to whileend1125 if zero.
// Leave LHS in D; ends at bool-done1130.
@multctr1112
D=M
// Move to lhs1126
@lhs1126
M=D
// Leave RHS in D.
@0
D=A
@rhs1127
M=D
// Subtract LHS from RHS
@lhs1126
D=M
@rhs1127
// These instructions are prescient ... 20130308
A=M
D=A-D
@one1129
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done1130
0;JMP
(one1129)
@1
D=A
// Done with boolean op.
(bool-done1130)
@whileend1125
D;JEQ
// Run body of whiletop1124.
// Set mulsum1113.
@mulsum1113
D=M
@lhs1131
M=D
@mullhs1114
D=M
@rhs1132
M=D
// Stick LHS result lhs1131 in D
@lhs1131
D=M
// Stick RHS result rhs1132 in A
@rhs1132
A=M
D=D+A
@mulsum1113
M=D
// Set multctr1112.
@multctr1112
D=M
@lhs1133
M=D
@1
D=A
@rhs1134
M=D
// Stick LHS result lhs1133 in D
@lhs1133
D=M
// Stick RHS result rhs1134 in A
@rhs1134
A=M
D=D-A
@multctr1112
M=D
// Go to top of loop.
@whiletop1124
0;JMP
// End of whiletop1124.
(whileend1125)
@mulsum1113
D=M
@rhs1094
M=D
// Stick LHS result lhs1093 in D
@lhs1093
D=M
// Stick RHS result rhs1094 in A
@rhs1094
A=M
D=D+A
@rhs1075
M=D
// Subtract LHS from RHS
@lhs1074
D=M
@rhs1075
// These instructions are prescient ... 20130308
A=M
D=A-D
@one1077
// Jump and leave 1 in D if true.
D;JGT
@0
D=A
@bool-done1078
0;JMP
(one1077)
@1
D=A
// Done with boolean op.
(bool-done1078)
// If the result was zero, we jump to iffalse1072
@iffalse1072
D;JEQ
@x73
D=M
// Jump to the ifend1073 when done with the true case.
@ifend1073
0;JMP
(iffalse1072)
@0
D=A
@x80
M=D
@x73
D=M
(ifend1073)
@rhs1058
M=D
// Stick LHS result lhs1057 in D
@lhs1057
D=M
// Stick RHS result rhs1058 in A
@rhs1058
A=M
D=D-A
@rhs946
M=D
// Subtract LHS from RHS
@lhs945
D=M
@rhs946
// These instructions are prescient ... 20130308
A=M
D=A-D
@one948
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done949
0;JMP
(one948)
@1
D=A
// Done with boolean op.
(bool-done949)
// If the result was zero, we jump to iffalse943
@iffalse943
D;JEQ
@8
D=A
// Jump to the ifend944 when done with the true case.
@ifend944
0;JMP
(iffalse943)
@6
D=A
(ifend944)
// Move D register to RAM[0] for final result.
@0
M=D
(ENDOFLINE)
@ENDOFLINE
0;JMP
