// Test 004
// Source: 
//
// (let ((x7 7)) (let ((x8 1)) (+ (let ((x9 4)) (* (+ 7 x9) (let ((x10 1)) (+ 5 0)))) (if (< x8 (let ((x11 0)) (let ((x12 6)) (let ((x13 0)) (let ((x14 9)) x8))))) (* x8 x8) x8))))
// 
// Result (in RAM0): 56
// Generated on 20130309 at 19:54.
// -----------------------------------//
@7
D=A
@x7
M=D
@1
D=A
@x8
M=D
@4
D=A
@x9
M=D
@7
D=A
@lhs191
M=D
@x9
D=M
@rhs192
M=D
// Stick LHS result lhs191 in D
@lhs191
D=M
// Stick RHS result rhs192 in A
@rhs192
A=M
D=D+A
@mullhs189
M=D
@1
D=A
@x10
M=D
@5
D=A
@lhs193
M=D
@0
D=A
@rhs194
M=D
// Stick LHS result lhs193 in D
@lhs193
D=M
// Stick RHS result rhs194 in A
@rhs194
A=M
D=D+A
@mulrhs190
M=D
// Set multctr187.
@mulrhs190
D=M
@multctr187
M=D
// Set mulsum188.
@0
D=A
@mulsum188
M=D
// Top of whiletop195
(whiletop195)
// Test; jump to whileend196 if zero.
// Leave LHS in D; ends at bool-done201.
@multctr187
D=M
// Move to lhs197
@lhs197
M=D
// Leave RHS in D.
@0
D=A
@rhs198
M=D
// Subtract LHS from RHS
@lhs197
D=M
@rhs198
// These instructions are prescient ... 20130308
A=M
D=A-D
@one200
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done201
0;JMP
(one200)
@1
D=A
// Done with boolean op.
(bool-done201)
@whileend196
D;JEQ
// Run body of whiletop195.
// Set mulsum188.
@mulsum188
D=M
@lhs202
M=D
@mullhs189
D=M
@rhs203
M=D
// Stick LHS result lhs202 in D
@lhs202
D=M
// Stick RHS result rhs203 in A
@rhs203
A=M
D=D+A
@mulsum188
M=D
// Set multctr187.
@multctr187
D=M
@lhs204
M=D
@1
D=A
@rhs205
M=D
// Stick LHS result lhs204 in D
@lhs204
D=M
// Stick RHS result rhs205 in A
@rhs205
A=M
D=D-A
@multctr187
M=D
// Go to top of loop.
@whiletop195
0;JMP
// End of whiletop195.
(whileend196)
@mulsum188
D=M
@lhs185
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done213.
@x8
D=M
// Move to lhs209
@lhs209
M=D
// Leave RHS in D.
@0
D=A
@x11
M=D
@6
D=A
@x12
M=D
@0
D=A
@x13
M=D
@9
D=A
@x14
M=D
@x8
D=M
@rhs210
M=D
// Subtract LHS from RHS
@lhs209
D=M
@rhs210
// These instructions are prescient ... 20130308
A=M
D=A-D
@one212
// Jump and leave 1 in D if true.
D;JGT
@0
D=A
@bool-done213
0;JMP
(one212)
@1
D=A
// Done with boolean op.
(bool-done213)
// If the result was zero, we jump to iffalse207
@iffalse207
D;JEQ
@x8
D=M
@mullhs216
M=D
@x8
D=M
@mulrhs217
M=D
// Set multctr214.
@mulrhs217
D=M
@multctr214
M=D
// Set mulsum215.
@0
D=A
@mulsum215
M=D
// Top of whiletop218
(whiletop218)
// Test; jump to whileend219 if zero.
// Leave LHS in D; ends at bool-done224.
@multctr214
D=M
// Move to lhs220
@lhs220
M=D
// Leave RHS in D.
@0
D=A
@rhs221
M=D
// Subtract LHS from RHS
@lhs220
D=M
@rhs221
// These instructions are prescient ... 20130308
A=M
D=A-D
@one223
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done224
0;JMP
(one223)
@1
D=A
// Done with boolean op.
(bool-done224)
@whileend219
D;JEQ
// Run body of whiletop218.
// Set mulsum215.
@mulsum215
D=M
@lhs225
M=D
@mullhs216
D=M
@rhs226
M=D
// Stick LHS result lhs225 in D
@lhs225
D=M
// Stick RHS result rhs226 in A
@rhs226
A=M
D=D+A
@mulsum215
M=D
// Set multctr214.
@multctr214
D=M
@lhs227
M=D
@1
D=A
@rhs228
M=D
// Stick LHS result lhs227 in D
@lhs227
D=M
// Stick RHS result rhs228 in A
@rhs228
A=M
D=D-A
@multctr214
M=D
// Go to top of loop.
@whiletop218
0;JMP
// End of whiletop218.
(whileend219)
@mulsum215
D=M
// Jump to the ifend208 when done with the true case.
@ifend208
0;JMP
(iffalse207)
@x8
D=M
(ifend208)
@rhs186
M=D
// Stick LHS result lhs185 in D
@lhs185
D=M
// Stick RHS result rhs186 in A
@rhs186
A=M
D=D+A
// Move D register to RAM[0] for final result.
@0
M=D
(ENDOFLINE)
@ENDOFLINE
0;JMP
