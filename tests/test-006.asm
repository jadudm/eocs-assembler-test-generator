// Test 006
// Source: 
//
// (let ((x22 3)) (+ x22 (if (< (- (if (> 1 5) x22 x22) (* x22 x22)) x22) (if (> x22 (+ x22 8)) 8 (if (> x22 x22) x22 x22)) x22)))
// 
// Result (in RAM0): 6
// Generated on 20130309 at 19:54.
// -----------------------------------//
@3
D=A
@x22
M=D
@x22
D=M
@lhs258
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done267.
// Do test, leave result in D
// Leave LHS in D; ends at bool-done277.
@1
D=A
// Move to lhs273
@lhs273
M=D
// Leave RHS in D.
@5
D=A
@rhs274
M=D
// Subtract LHS from RHS
@lhs273
D=M
@rhs274
// These instructions are prescient ... 20130308
A=M
D=A-D
@one276
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done277
0;JMP
(one276)
@1
D=A
// Done with boolean op.
(bool-done277)
// If the result was zero, we jump to iffalse271
@iffalse271
D;JEQ
@x22
D=M
// Jump to the ifend272 when done with the true case.
@ifend272
0;JMP
(iffalse271)
@x22
D=M
(ifend272)
@lhs268
M=D
@x22
D=M
@mullhs280
M=D
@x22
D=M
@mulrhs281
M=D
// Set multctr278.
@mulrhs281
D=M
@multctr278
M=D
// Set mulsum279.
@0
D=A
@mulsum279
M=D
// Top of whiletop282
(whiletop282)
// Test; jump to whileend283 if zero.
// Leave LHS in D; ends at bool-done288.
@multctr278
D=M
// Move to lhs284
@lhs284
M=D
// Leave RHS in D.
@0
D=A
@rhs285
M=D
// Subtract LHS from RHS
@lhs284
D=M
@rhs285
// These instructions are prescient ... 20130308
A=M
D=A-D
@one287
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done288
0;JMP
(one287)
@1
D=A
// Done with boolean op.
(bool-done288)
@whileend283
D;JEQ
// Run body of whiletop282.
// Set mulsum279.
@mulsum279
D=M
@lhs289
M=D
@mullhs280
D=M
@rhs290
M=D
// Stick LHS result lhs289 in D
@lhs289
D=M
// Stick RHS result rhs290 in A
@rhs290
A=M
D=D+A
@mulsum279
M=D
// Set multctr278.
@multctr278
D=M
@lhs291
M=D
@1
D=A
@rhs292
M=D
// Stick LHS result lhs291 in D
@lhs291
D=M
// Stick RHS result rhs292 in A
@rhs292
A=M
D=D-A
@multctr278
M=D
// Go to top of loop.
@whiletop282
0;JMP
// End of whiletop282.
(whileend283)
@mulsum279
D=M
@rhs269
M=D
// Stick LHS result lhs268 in D
@lhs268
D=M
// Stick RHS result rhs269 in A
@rhs269
A=M
D=D-A
// Move to lhs263
@lhs263
M=D
// Leave RHS in D.
@x22
D=M
@rhs264
M=D
// Subtract LHS from RHS
@lhs263
D=M
@rhs264
// These instructions are prescient ... 20130308
A=M
D=A-D
@one266
// Jump and leave 1 in D if true.
D;JGT
@0
D=A
@bool-done267
0;JMP
(one266)
@1
D=A
// Done with boolean op.
(bool-done267)
// If the result was zero, we jump to iffalse261
@iffalse261
D;JEQ
// Do test, leave result in D
// Leave LHS in D; ends at bool-done300.
@x22
D=M
// Move to lhs296
@lhs296
M=D
// Leave RHS in D.
@x22
D=M
@lhs301
M=D
@8
D=A
@rhs302
M=D
// Stick LHS result lhs301 in D
@lhs301
D=M
// Stick RHS result rhs302 in A
@rhs302
A=M
D=D+A
@rhs297
M=D
// Subtract LHS from RHS
@lhs296
D=M
@rhs297
// These instructions are prescient ... 20130308
A=M
D=A-D
@one299
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done300
0;JMP
(one299)
@1
D=A
// Done with boolean op.
(bool-done300)
// If the result was zero, we jump to iffalse294
@iffalse294
D;JEQ
@8
D=A
// Jump to the ifend295 when done with the true case.
@ifend295
0;JMP
(iffalse294)
// Do test, leave result in D
// Leave LHS in D; ends at bool-done310.
@x22
D=M
// Move to lhs306
@lhs306
M=D
// Leave RHS in D.
@x22
D=M
@rhs307
M=D
// Subtract LHS from RHS
@lhs306
D=M
@rhs307
// These instructions are prescient ... 20130308
A=M
D=A-D
@one309
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done310
0;JMP
(one309)
@1
D=A
// Done with boolean op.
(bool-done310)
// If the result was zero, we jump to iffalse304
@iffalse304
D;JEQ
@x22
D=M
// Jump to the ifend305 when done with the true case.
@ifend305
0;JMP
(iffalse304)
@x22
D=M
(ifend305)
(ifend295)
// Jump to the ifend262 when done with the true case.
@ifend262
0;JMP
(iffalse261)
@x22
D=M
(ifend262)
@rhs259
M=D
// Stick LHS result lhs258 in D
@lhs258
D=M
// Stick RHS result rhs259 in A
@rhs259
A=M
D=D+A
// Move D register to RAM[0] for final result.
@0
M=D
(ENDOFLINE)
@ENDOFLINE
0;JMP
