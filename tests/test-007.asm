// Test 007
// Source: 
//
// (let ((x23 7)) (let ((x24 5)) 6))
// 
// Result (in RAM0): 6
// Generated on 20130309 at 19:54.
// -----------------------------------//
@7
D=A
@x23
M=D
@5
D=A
@x24
M=D
@6
D=A
// Move D register to RAM[0] for final result.
@0
M=D
(ENDOFLINE)
@ENDOFLINE
0;JMP
