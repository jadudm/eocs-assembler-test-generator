// Test 003
// Source: 
//
// (let ((x2 9)) (if (> (let ((x3 2)) (* (if (< x2 x3) x3 x2) (let ((x4 0)) (- x4 x2)))) (let ((x5 0)) x2)) (+ (let ((x6 7)) (+ 2 5)) (+ x2 7)) (* x2 x2)))
// 
// Result (in RAM0): 81
// Generated on 20130309 at 19:54.
// -----------------------------------//
@9
D=A
@x2
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done138.
@2
D=A
@x3
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done150.
@x2
D=M
// Move to lhs146
@lhs146
M=D
// Leave RHS in D.
@x3
D=M
@rhs147
M=D
// Subtract LHS from RHS
@lhs146
D=M
@rhs147
// These instructions are prescient ... 20130308
A=M
D=A-D
@one149
// Jump and leave 1 in D if true.
D;JGT
@0
D=A
@bool-done150
0;JMP
(one149)
@1
D=A
// Done with boolean op.
(bool-done150)
// If the result was zero, we jump to iffalse144
@iffalse144
D;JEQ
@x3
D=M
// Jump to the ifend145 when done with the true case.
@ifend145
0;JMP
(iffalse144)
@x2
D=M
(ifend145)
@mullhs141
M=D
@0
D=A
@x4
M=D
@x4
D=M
@lhs151
M=D
@x2
D=M
@rhs152
M=D
// Stick LHS result lhs151 in D
@lhs151
D=M
// Stick RHS result rhs152 in A
@rhs152
A=M
D=D-A
@mulrhs142
M=D
// Set multctr139.
@mulrhs142
D=M
@multctr139
M=D
// Set mulsum140.
@0
D=A
@mulsum140
M=D
// Top of whiletop153
(whiletop153)
// Test; jump to whileend154 if zero.
// Leave LHS in D; ends at bool-done159.
@multctr139
D=M
// Move to lhs155
@lhs155
M=D
// Leave RHS in D.
@0
D=A
@rhs156
M=D
// Subtract LHS from RHS
@lhs155
D=M
@rhs156
// These instructions are prescient ... 20130308
A=M
D=A-D
@one158
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done159
0;JMP
(one158)
@1
D=A
// Done with boolean op.
(bool-done159)
@whileend154
D;JEQ
// Run body of whiletop153.
// Set mulsum140.
@mulsum140
D=M
@lhs160
M=D
@mullhs141
D=M
@rhs161
M=D
// Stick LHS result lhs160 in D
@lhs160
D=M
// Stick RHS result rhs161 in A
@rhs161
A=M
D=D+A
@mulsum140
M=D
// Set multctr139.
@multctr139
D=M
@lhs162
M=D
@1
D=A
@rhs163
M=D
// Stick LHS result lhs162 in D
@lhs162
D=M
// Stick RHS result rhs163 in A
@rhs163
A=M
D=D-A
@multctr139
M=D
// Go to top of loop.
@whiletop153
0;JMP
// End of whiletop153.
(whileend154)
@mulsum140
D=M
// Move to lhs134
@lhs134
M=D
// Leave RHS in D.
@0
D=A
@x5
M=D
@x2
D=M
@rhs135
M=D
// Subtract LHS from RHS
@lhs134
D=M
@rhs135
// These instructions are prescient ... 20130308
A=M
D=A-D
@one137
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done138
0;JMP
(one137)
@1
D=A
// Done with boolean op.
(bool-done138)
// If the result was zero, we jump to iffalse132
@iffalse132
D;JEQ
@7
D=A
@x6
M=D
@2
D=A
@lhs166
M=D
@5
D=A
@rhs167
M=D
// Stick LHS result lhs166 in D
@lhs166
D=M
// Stick RHS result rhs167 in A
@rhs167
A=M
D=D+A
@lhs164
M=D
@x2
D=M
@lhs168
M=D
@7
D=A
@rhs169
M=D
// Stick LHS result lhs168 in D
@lhs168
D=M
// Stick RHS result rhs169 in A
@rhs169
A=M
D=D+A
@rhs165
M=D
// Stick LHS result lhs164 in D
@lhs164
D=M
// Stick RHS result rhs165 in A
@rhs165
A=M
D=D+A
// Jump to the ifend133 when done with the true case.
@ifend133
0;JMP
(iffalse132)
@x2
D=M
@mullhs172
M=D
@x2
D=M
@mulrhs173
M=D
// Set multctr170.
@mulrhs173
D=M
@multctr170
M=D
// Set mulsum171.
@0
D=A
@mulsum171
M=D
// Top of whiletop174
(whiletop174)
// Test; jump to whileend175 if zero.
// Leave LHS in D; ends at bool-done180.
@multctr170
D=M
// Move to lhs176
@lhs176
M=D
// Leave RHS in D.
@0
D=A
@rhs177
M=D
// Subtract LHS from RHS
@lhs176
D=M
@rhs177
// These instructions are prescient ... 20130308
A=M
D=A-D
@one179
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done180
0;JMP
(one179)
@1
D=A
// Done with boolean op.
(bool-done180)
@whileend175
D;JEQ
// Run body of whiletop174.
// Set mulsum171.
@mulsum171
D=M
@lhs181
M=D
@mullhs172
D=M
@rhs182
M=D
// Stick LHS result lhs181 in D
@lhs181
D=M
// Stick RHS result rhs182 in A
@rhs182
A=M
D=D+A
@mulsum171
M=D
// Set multctr170.
@multctr170
D=M
@lhs183
M=D
@1
D=A
@rhs184
M=D
// Stick LHS result lhs183 in D
@lhs183
D=M
// Stick RHS result rhs184 in A
@rhs184
A=M
D=D-A
@multctr170
M=D
// Go to top of loop.
@whiletop174
0;JMP
// End of whiletop174.
(whileend175)
@mulsum171
D=M
(ifend133)
// Move D register to RAM[0] for final result.
@0
M=D
(ENDOFLINE)
@ENDOFLINE
0;JMP
