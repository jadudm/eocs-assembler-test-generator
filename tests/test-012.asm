// Test 012
// Source: 
//
// (if (< (let ((x54 8)) (- (* 4 (+ (* 9 1) x54)) (- (let ((x55 4)) (* (let ((x56 7)) (if (> x56 7) x56 6)) (- 9 x54))) (if (< x54 (let ((x57 5)) (if (> 1 9) 4 x57))) (if (< x54 x54) 3 x54) 8)))) 0) (if (> (if (< 7 (if (< 2 4) 8 7)) 5 (if (> (- 8 1) (* 8 6)) (if (< 0 3) 4 7) 2)) (* (- (- 7 6) 0) 7)) 7 (+ (+ 5 (let ((x58 4)) (let ((x59 7)) (let ((x60 7)) (- x58 7))))) (let ((x61 8)) (+ (if (> 4 4) 7 4) (* x61 3))))) (- (let ((x62 2)) (+ (let ((x63 3)) (if (> (- x63 x63) (+ 5 x62)) x62 (+ 3 x62))) (let ((x64 7)) 0))) (if (> 2 0) (+ (* 4 8) (if (> 4 6) 3 6)) (let ((x65 0)) (+ (* x65 x65) (* 7 x65))))))
// 
// Result (in RAM0): -33
// Generated on 20130309 at 19:54.
// -----------------------------------//
// Do test, leave result in D
// Leave LHS in D; ends at bool-done664.
@8
D=A
@x54
M=D
@4
D=A
@mullhs669
M=D
@9
D=A
@mullhs675
M=D
@1
D=A
@mulrhs676
M=D
// Set multctr673.
@mulrhs676
D=M
@multctr673
M=D
// Set mulsum674.
@0
D=A
@mulsum674
M=D
// Top of whiletop677
(whiletop677)
// Test; jump to whileend678 if zero.
// Leave LHS in D; ends at bool-done683.
@multctr673
D=M
// Move to lhs679
@lhs679
M=D
// Leave RHS in D.
@0
D=A
@rhs680
M=D
// Subtract LHS from RHS
@lhs679
D=M
@rhs680
// These instructions are prescient ... 20130308
A=M
D=A-D
@one682
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done683
0;JMP
(one682)
@1
D=A
// Done with boolean op.
(bool-done683)
@whileend678
D;JEQ
// Run body of whiletop677.
// Set mulsum674.
@mulsum674
D=M
@lhs684
M=D
@mullhs675
D=M
@rhs685
M=D
// Stick LHS result lhs684 in D
@lhs684
D=M
// Stick RHS result rhs685 in A
@rhs685
A=M
D=D+A
@mulsum674
M=D
// Set multctr673.
@multctr673
D=M
@lhs686
M=D
@1
D=A
@rhs687
M=D
// Stick LHS result lhs686 in D
@lhs686
D=M
// Stick RHS result rhs687 in A
@rhs687
A=M
D=D-A
@multctr673
M=D
// Go to top of loop.
@whiletop677
0;JMP
// End of whiletop677.
(whileend678)
@mulsum674
D=M
@lhs671
M=D
@x54
D=M
@rhs672
M=D
// Stick LHS result lhs671 in D
@lhs671
D=M
// Stick RHS result rhs672 in A
@rhs672
A=M
D=D+A
@mulrhs670
M=D
// Set multctr667.
@mulrhs670
D=M
@multctr667
M=D
// Set mulsum668.
@0
D=A
@mulsum668
M=D
// Top of whiletop688
(whiletop688)
// Test; jump to whileend689 if zero.
// Leave LHS in D; ends at bool-done694.
@multctr667
D=M
// Move to lhs690
@lhs690
M=D
// Leave RHS in D.
@0
D=A
@rhs691
M=D
// Subtract LHS from RHS
@lhs690
D=M
@rhs691
// These instructions are prescient ... 20130308
A=M
D=A-D
@one693
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done694
0;JMP
(one693)
@1
D=A
// Done with boolean op.
(bool-done694)
@whileend689
D;JEQ
// Run body of whiletop688.
// Set mulsum668.
@mulsum668
D=M
@lhs695
M=D
@mullhs669
D=M
@rhs696
M=D
// Stick LHS result lhs695 in D
@lhs695
D=M
// Stick RHS result rhs696 in A
@rhs696
A=M
D=D+A
@mulsum668
M=D
// Set multctr667.
@multctr667
D=M
@lhs697
M=D
@1
D=A
@rhs698
M=D
// Stick LHS result lhs697 in D
@lhs697
D=M
// Stick RHS result rhs698 in A
@rhs698
A=M
D=D-A
@multctr667
M=D
// Go to top of loop.
@whiletop688
0;JMP
// End of whiletop688.
(whileend689)
@mulsum668
D=M
@lhs665
M=D
@4
D=A
@x55
M=D
@7
D=A
@x56
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done712.
@x56
D=M
// Move to lhs708
@lhs708
M=D
// Leave RHS in D.
@7
D=A
@rhs709
M=D
// Subtract LHS from RHS
@lhs708
D=M
@rhs709
// These instructions are prescient ... 20130308
A=M
D=A-D
@one711
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done712
0;JMP
(one711)
@1
D=A
// Done with boolean op.
(bool-done712)
// If the result was zero, we jump to iffalse706
@iffalse706
D;JEQ
@x56
D=M
// Jump to the ifend707 when done with the true case.
@ifend707
0;JMP
(iffalse706)
@6
D=A
(ifend707)
@mullhs703
M=D
@9
D=A
@lhs713
M=D
@x54
D=M
@rhs714
M=D
// Stick LHS result lhs713 in D
@lhs713
D=M
// Stick RHS result rhs714 in A
@rhs714
A=M
D=D-A
@mulrhs704
M=D
// Set multctr701.
@mulrhs704
D=M
@multctr701
M=D
// Set mulsum702.
@0
D=A
@mulsum702
M=D
// Top of whiletop715
(whiletop715)
// Test; jump to whileend716 if zero.
// Leave LHS in D; ends at bool-done721.
@multctr701
D=M
// Move to lhs717
@lhs717
M=D
// Leave RHS in D.
@0
D=A
@rhs718
M=D
// Subtract LHS from RHS
@lhs717
D=M
@rhs718
// These instructions are prescient ... 20130308
A=M
D=A-D
@one720
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done721
0;JMP
(one720)
@1
D=A
// Done with boolean op.
(bool-done721)
@whileend716
D;JEQ
// Run body of whiletop715.
// Set mulsum702.
@mulsum702
D=M
@lhs722
M=D
@mullhs703
D=M
@rhs723
M=D
// Stick LHS result lhs722 in D
@lhs722
D=M
// Stick RHS result rhs723 in A
@rhs723
A=M
D=D+A
@mulsum702
M=D
// Set multctr701.
@multctr701
D=M
@lhs724
M=D
@1
D=A
@rhs725
M=D
// Stick LHS result lhs724 in D
@lhs724
D=M
// Stick RHS result rhs725 in A
@rhs725
A=M
D=D-A
@multctr701
M=D
// Go to top of loop.
@whiletop715
0;JMP
// End of whiletop715.
(whileend716)
@mulsum702
D=M
@lhs699
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done733.
@x54
D=M
// Move to lhs729
@lhs729
M=D
// Leave RHS in D.
@5
D=A
@x57
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done741.
@1
D=A
// Move to lhs737
@lhs737
M=D
// Leave RHS in D.
@9
D=A
@rhs738
M=D
// Subtract LHS from RHS
@lhs737
D=M
@rhs738
// These instructions are prescient ... 20130308
A=M
D=A-D
@one740
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done741
0;JMP
(one740)
@1
D=A
// Done with boolean op.
(bool-done741)
// If the result was zero, we jump to iffalse735
@iffalse735
D;JEQ
@4
D=A
// Jump to the ifend736 when done with the true case.
@ifend736
0;JMP
(iffalse735)
@x57
D=M
(ifend736)
@rhs730
M=D
// Subtract LHS from RHS
@lhs729
D=M
@rhs730
// These instructions are prescient ... 20130308
A=M
D=A-D
@one732
// Jump and leave 1 in D if true.
D;JGT
@0
D=A
@bool-done733
0;JMP
(one732)
@1
D=A
// Done with boolean op.
(bool-done733)
// If the result was zero, we jump to iffalse727
@iffalse727
D;JEQ
// Do test, leave result in D
// Leave LHS in D; ends at bool-done749.
@x54
D=M
// Move to lhs745
@lhs745
M=D
// Leave RHS in D.
@x54
D=M
@rhs746
M=D
// Subtract LHS from RHS
@lhs745
D=M
@rhs746
// These instructions are prescient ... 20130308
A=M
D=A-D
@one748
// Jump and leave 1 in D if true.
D;JGT
@0
D=A
@bool-done749
0;JMP
(one748)
@1
D=A
// Done with boolean op.
(bool-done749)
// If the result was zero, we jump to iffalse743
@iffalse743
D;JEQ
@3
D=A
// Jump to the ifend744 when done with the true case.
@ifend744
0;JMP
(iffalse743)
@x54
D=M
(ifend744)
// Jump to the ifend728 when done with the true case.
@ifend728
0;JMP
(iffalse727)
@8
D=A
(ifend728)
@rhs700
M=D
// Stick LHS result lhs699 in D
@lhs699
D=M
// Stick RHS result rhs700 in A
@rhs700
A=M
D=D-A
@rhs666
M=D
// Stick LHS result lhs665 in D
@lhs665
D=M
// Stick RHS result rhs666 in A
@rhs666
A=M
D=D-A
// Move to lhs660
@lhs660
M=D
// Leave RHS in D.
@0
D=A
@rhs661
M=D
// Subtract LHS from RHS
@lhs660
D=M
@rhs661
// These instructions are prescient ... 20130308
A=M
D=A-D
@one663
// Jump and leave 1 in D if true.
D;JGT
@0
D=A
@bool-done664
0;JMP
(one663)
@1
D=A
// Done with boolean op.
(bool-done664)
// If the result was zero, we jump to iffalse658
@iffalse658
D;JEQ
// Do test, leave result in D
// Leave LHS in D; ends at bool-done757.
// Do test, leave result in D
// Leave LHS in D; ends at bool-done765.
@7
D=A
// Move to lhs761
@lhs761
M=D
// Leave RHS in D.
// Do test, leave result in D
// Leave LHS in D; ends at bool-done773.
@2
D=A
// Move to lhs769
@lhs769
M=D
// Leave RHS in D.
@4
D=A
@rhs770
M=D
// Subtract LHS from RHS
@lhs769
D=M
@rhs770
// These instructions are prescient ... 20130308
A=M
D=A-D
@one772
// Jump and leave 1 in D if true.
D;JGT
@0
D=A
@bool-done773
0;JMP
(one772)
@1
D=A
// Done with boolean op.
(bool-done773)
// If the result was zero, we jump to iffalse767
@iffalse767
D;JEQ
@8
D=A
// Jump to the ifend768 when done with the true case.
@ifend768
0;JMP
(iffalse767)
@7
D=A
(ifend768)
@rhs762
M=D
// Subtract LHS from RHS
@lhs761
D=M
@rhs762
// These instructions are prescient ... 20130308
A=M
D=A-D
@one764
// Jump and leave 1 in D if true.
D;JGT
@0
D=A
@bool-done765
0;JMP
(one764)
@1
D=A
// Done with boolean op.
(bool-done765)
// If the result was zero, we jump to iffalse759
@iffalse759
D;JEQ
@5
D=A
// Jump to the ifend760 when done with the true case.
@ifend760
0;JMP
(iffalse759)
// Do test, leave result in D
// Leave LHS in D; ends at bool-done781.
@8
D=A
@lhs782
M=D
@1
D=A
@rhs783
M=D
// Stick LHS result lhs782 in D
@lhs782
D=M
// Stick RHS result rhs783 in A
@rhs783
A=M
D=D-A
// Move to lhs777
@lhs777
M=D
// Leave RHS in D.
@8
D=A
@mullhs786
M=D
@6
D=A
@mulrhs787
M=D
// Set multctr784.
@mulrhs787
D=M
@multctr784
M=D
// Set mulsum785.
@0
D=A
@mulsum785
M=D
// Top of whiletop788
(whiletop788)
// Test; jump to whileend789 if zero.
// Leave LHS in D; ends at bool-done794.
@multctr784
D=M
// Move to lhs790
@lhs790
M=D
// Leave RHS in D.
@0
D=A
@rhs791
M=D
// Subtract LHS from RHS
@lhs790
D=M
@rhs791
// These instructions are prescient ... 20130308
A=M
D=A-D
@one793
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done794
0;JMP
(one793)
@1
D=A
// Done with boolean op.
(bool-done794)
@whileend789
D;JEQ
// Run body of whiletop788.
// Set mulsum785.
@mulsum785
D=M
@lhs795
M=D
@mullhs786
D=M
@rhs796
M=D
// Stick LHS result lhs795 in D
@lhs795
D=M
// Stick RHS result rhs796 in A
@rhs796
A=M
D=D+A
@mulsum785
M=D
// Set multctr784.
@multctr784
D=M
@lhs797
M=D
@1
D=A
@rhs798
M=D
// Stick LHS result lhs797 in D
@lhs797
D=M
// Stick RHS result rhs798 in A
@rhs798
A=M
D=D-A
@multctr784
M=D
// Go to top of loop.
@whiletop788
0;JMP
// End of whiletop788.
(whileend789)
@mulsum785
D=M
@rhs778
M=D
// Subtract LHS from RHS
@lhs777
D=M
@rhs778
// These instructions are prescient ... 20130308
A=M
D=A-D
@one780
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done781
0;JMP
(one780)
@1
D=A
// Done with boolean op.
(bool-done781)
// If the result was zero, we jump to iffalse775
@iffalse775
D;JEQ
// Do test, leave result in D
// Leave LHS in D; ends at bool-done806.
@0
D=A
// Move to lhs802
@lhs802
M=D
// Leave RHS in D.
@3
D=A
@rhs803
M=D
// Subtract LHS from RHS
@lhs802
D=M
@rhs803
// These instructions are prescient ... 20130308
A=M
D=A-D
@one805
// Jump and leave 1 in D if true.
D;JGT
@0
D=A
@bool-done806
0;JMP
(one805)
@1
D=A
// Done with boolean op.
(bool-done806)
// If the result was zero, we jump to iffalse800
@iffalse800
D;JEQ
@4
D=A
// Jump to the ifend801 when done with the true case.
@ifend801
0;JMP
(iffalse800)
@7
D=A
(ifend801)
// Jump to the ifend776 when done with the true case.
@ifend776
0;JMP
(iffalse775)
@2
D=A
(ifend776)
(ifend760)
// Move to lhs753
@lhs753
M=D
// Leave RHS in D.
@7
D=A
@lhs813
M=D
@6
D=A
@rhs814
M=D
// Stick LHS result lhs813 in D
@lhs813
D=M
// Stick RHS result rhs814 in A
@rhs814
A=M
D=D-A
@lhs811
M=D
@0
D=A
@rhs812
M=D
// Stick LHS result lhs811 in D
@lhs811
D=M
// Stick RHS result rhs812 in A
@rhs812
A=M
D=D-A
@mullhs809
M=D
@7
D=A
@mulrhs810
M=D
// Set multctr807.
@mulrhs810
D=M
@multctr807
M=D
// Set mulsum808.
@0
D=A
@mulsum808
M=D
// Top of whiletop815
(whiletop815)
// Test; jump to whileend816 if zero.
// Leave LHS in D; ends at bool-done821.
@multctr807
D=M
// Move to lhs817
@lhs817
M=D
// Leave RHS in D.
@0
D=A
@rhs818
M=D
// Subtract LHS from RHS
@lhs817
D=M
@rhs818
// These instructions are prescient ... 20130308
A=M
D=A-D
@one820
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done821
0;JMP
(one820)
@1
D=A
// Done with boolean op.
(bool-done821)
@whileend816
D;JEQ
// Run body of whiletop815.
// Set mulsum808.
@mulsum808
D=M
@lhs822
M=D
@mullhs809
D=M
@rhs823
M=D
// Stick LHS result lhs822 in D
@lhs822
D=M
// Stick RHS result rhs823 in A
@rhs823
A=M
D=D+A
@mulsum808
M=D
// Set multctr807.
@multctr807
D=M
@lhs824
M=D
@1
D=A
@rhs825
M=D
// Stick LHS result lhs824 in D
@lhs824
D=M
// Stick RHS result rhs825 in A
@rhs825
A=M
D=D-A
@multctr807
M=D
// Go to top of loop.
@whiletop815
0;JMP
// End of whiletop815.
(whileend816)
@mulsum808
D=M
@rhs754
M=D
// Subtract LHS from RHS
@lhs753
D=M
@rhs754
// These instructions are prescient ... 20130308
A=M
D=A-D
@one756
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done757
0;JMP
(one756)
@1
D=A
// Done with boolean op.
(bool-done757)
// If the result was zero, we jump to iffalse751
@iffalse751
D;JEQ
@7
D=A
// Jump to the ifend752 when done with the true case.
@ifend752
0;JMP
(iffalse751)
@5
D=A
@lhs828
M=D
@4
D=A
@x58
M=D
@7
D=A
@x59
M=D
@7
D=A
@x60
M=D
@x58
D=M
@lhs830
M=D
@7
D=A
@rhs831
M=D
// Stick LHS result lhs830 in D
@lhs830
D=M
// Stick RHS result rhs831 in A
@rhs831
A=M
D=D-A
@rhs829
M=D
// Stick LHS result lhs828 in D
@lhs828
D=M
// Stick RHS result rhs829 in A
@rhs829
A=M
D=D+A
@lhs826
M=D
@8
D=A
@x61
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done841.
@4
D=A
// Move to lhs837
@lhs837
M=D
// Leave RHS in D.
@4
D=A
@rhs838
M=D
// Subtract LHS from RHS
@lhs837
D=M
@rhs838
// These instructions are prescient ... 20130308
A=M
D=A-D
@one840
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done841
0;JMP
(one840)
@1
D=A
// Done with boolean op.
(bool-done841)
// If the result was zero, we jump to iffalse835
@iffalse835
D;JEQ
@7
D=A
// Jump to the ifend836 when done with the true case.
@ifend836
0;JMP
(iffalse835)
@4
D=A
(ifend836)
@lhs832
M=D
@x61
D=M
@mullhs844
M=D
@3
D=A
@mulrhs845
M=D
// Set multctr842.
@mulrhs845
D=M
@multctr842
M=D
// Set mulsum843.
@0
D=A
@mulsum843
M=D
// Top of whiletop846
(whiletop846)
// Test; jump to whileend847 if zero.
// Leave LHS in D; ends at bool-done852.
@multctr842
D=M
// Move to lhs848
@lhs848
M=D
// Leave RHS in D.
@0
D=A
@rhs849
M=D
// Subtract LHS from RHS
@lhs848
D=M
@rhs849
// These instructions are prescient ... 20130308
A=M
D=A-D
@one851
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done852
0;JMP
(one851)
@1
D=A
// Done with boolean op.
(bool-done852)
@whileend847
D;JEQ
// Run body of whiletop846.
// Set mulsum843.
@mulsum843
D=M
@lhs853
M=D
@mullhs844
D=M
@rhs854
M=D
// Stick LHS result lhs853 in D
@lhs853
D=M
// Stick RHS result rhs854 in A
@rhs854
A=M
D=D+A
@mulsum843
M=D
// Set multctr842.
@multctr842
D=M
@lhs855
M=D
@1
D=A
@rhs856
M=D
// Stick LHS result lhs855 in D
@lhs855
D=M
// Stick RHS result rhs856 in A
@rhs856
A=M
D=D-A
@multctr842
M=D
// Go to top of loop.
@whiletop846
0;JMP
// End of whiletop846.
(whileend847)
@mulsum843
D=M
@rhs833
M=D
// Stick LHS result lhs832 in D
@lhs832
D=M
// Stick RHS result rhs833 in A
@rhs833
A=M
D=D+A
@rhs827
M=D
// Stick LHS result lhs826 in D
@lhs826
D=M
// Stick RHS result rhs827 in A
@rhs827
A=M
D=D+A
(ifend752)
// Jump to the ifend659 when done with the true case.
@ifend659
0;JMP
(iffalse658)
@2
D=A
@x62
M=D
@3
D=A
@x63
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done868.
@x63
D=M
@lhs869
M=D
@x63
D=M
@rhs870
M=D
// Stick LHS result lhs869 in D
@lhs869
D=M
// Stick RHS result rhs870 in A
@rhs870
A=M
D=D-A
// Move to lhs864
@lhs864
M=D
// Leave RHS in D.
@5
D=A
@lhs871
M=D
@x62
D=M
@rhs872
M=D
// Stick LHS result lhs871 in D
@lhs871
D=M
// Stick RHS result rhs872 in A
@rhs872
A=M
D=D+A
@rhs865
M=D
// Subtract LHS from RHS
@lhs864
D=M
@rhs865
// These instructions are prescient ... 20130308
A=M
D=A-D
@one867
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done868
0;JMP
(one867)
@1
D=A
// Done with boolean op.
(bool-done868)
// If the result was zero, we jump to iffalse862
@iffalse862
D;JEQ
@x62
D=M
// Jump to the ifend863 when done with the true case.
@ifend863
0;JMP
(iffalse862)
@3
D=A
@lhs873
M=D
@x62
D=M
@rhs874
M=D
// Stick LHS result lhs873 in D
@lhs873
D=M
// Stick RHS result rhs874 in A
@rhs874
A=M
D=D+A
(ifend863)
@lhs859
M=D
@7
D=A
@x64
M=D
@0
D=A
@rhs860
M=D
// Stick LHS result lhs859 in D
@lhs859
D=M
// Stick RHS result rhs860 in A
@rhs860
A=M
D=D+A
@lhs857
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done882.
@2
D=A
// Move to lhs878
@lhs878
M=D
// Leave RHS in D.
@0
D=A
@rhs879
M=D
// Subtract LHS from RHS
@lhs878
D=M
@rhs879
// These instructions are prescient ... 20130308
A=M
D=A-D
@one881
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done882
0;JMP
(one881)
@1
D=A
// Done with boolean op.
(bool-done882)
// If the result was zero, we jump to iffalse876
@iffalse876
D;JEQ
@4
D=A
@mullhs887
M=D
@8
D=A
@mulrhs888
M=D
// Set multctr885.
@mulrhs888
D=M
@multctr885
M=D
// Set mulsum886.
@0
D=A
@mulsum886
M=D
// Top of whiletop889
(whiletop889)
// Test; jump to whileend890 if zero.
// Leave LHS in D; ends at bool-done895.
@multctr885
D=M
// Move to lhs891
@lhs891
M=D
// Leave RHS in D.
@0
D=A
@rhs892
M=D
// Subtract LHS from RHS
@lhs891
D=M
@rhs892
// These instructions are prescient ... 20130308
A=M
D=A-D
@one894
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done895
0;JMP
(one894)
@1
D=A
// Done with boolean op.
(bool-done895)
@whileend890
D;JEQ
// Run body of whiletop889.
// Set mulsum886.
@mulsum886
D=M
@lhs896
M=D
@mullhs887
D=M
@rhs897
M=D
// Stick LHS result lhs896 in D
@lhs896
D=M
// Stick RHS result rhs897 in A
@rhs897
A=M
D=D+A
@mulsum886
M=D
// Set multctr885.
@multctr885
D=M
@lhs898
M=D
@1
D=A
@rhs899
M=D
// Stick LHS result lhs898 in D
@lhs898
D=M
// Stick RHS result rhs899 in A
@rhs899
A=M
D=D-A
@multctr885
M=D
// Go to top of loop.
@whiletop889
0;JMP
// End of whiletop889.
(whileend890)
@mulsum886
D=M
@lhs883
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done907.
@4
D=A
// Move to lhs903
@lhs903
M=D
// Leave RHS in D.
@6
D=A
@rhs904
M=D
// Subtract LHS from RHS
@lhs903
D=M
@rhs904
// These instructions are prescient ... 20130308
A=M
D=A-D
@one906
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done907
0;JMP
(one906)
@1
D=A
// Done with boolean op.
(bool-done907)
// If the result was zero, we jump to iffalse901
@iffalse901
D;JEQ
@3
D=A
// Jump to the ifend902 when done with the true case.
@ifend902
0;JMP
(iffalse901)
@6
D=A
(ifend902)
@rhs884
M=D
// Stick LHS result lhs883 in D
@lhs883
D=M
// Stick RHS result rhs884 in A
@rhs884
A=M
D=D+A
// Jump to the ifend877 when done with the true case.
@ifend877
0;JMP
(iffalse876)
@0
D=A
@x65
M=D
@x65
D=M
@mullhs912
M=D
@x65
D=M
@mulrhs913
M=D
// Set multctr910.
@mulrhs913
D=M
@multctr910
M=D
// Set mulsum911.
@0
D=A
@mulsum911
M=D
// Top of whiletop914
(whiletop914)
// Test; jump to whileend915 if zero.
// Leave LHS in D; ends at bool-done920.
@multctr910
D=M
// Move to lhs916
@lhs916
M=D
// Leave RHS in D.
@0
D=A
@rhs917
M=D
// Subtract LHS from RHS
@lhs916
D=M
@rhs917
// These instructions are prescient ... 20130308
A=M
D=A-D
@one919
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done920
0;JMP
(one919)
@1
D=A
// Done with boolean op.
(bool-done920)
@whileend915
D;JEQ
// Run body of whiletop914.
// Set mulsum911.
@mulsum911
D=M
@lhs921
M=D
@mullhs912
D=M
@rhs922
M=D
// Stick LHS result lhs921 in D
@lhs921
D=M
// Stick RHS result rhs922 in A
@rhs922
A=M
D=D+A
@mulsum911
M=D
// Set multctr910.
@multctr910
D=M
@lhs923
M=D
@1
D=A
@rhs924
M=D
// Stick LHS result lhs923 in D
@lhs923
D=M
// Stick RHS result rhs924 in A
@rhs924
A=M
D=D-A
@multctr910
M=D
// Go to top of loop.
@whiletop914
0;JMP
// End of whiletop914.
(whileend915)
@mulsum911
D=M
@lhs908
M=D
@7
D=A
@mullhs927
M=D
@x65
D=M
@mulrhs928
M=D
// Set multctr925.
@mulrhs928
D=M
@multctr925
M=D
// Set mulsum926.
@0
D=A
@mulsum926
M=D
// Top of whiletop929
(whiletop929)
// Test; jump to whileend930 if zero.
// Leave LHS in D; ends at bool-done935.
@multctr925
D=M
// Move to lhs931
@lhs931
M=D
// Leave RHS in D.
@0
D=A
@rhs932
M=D
// Subtract LHS from RHS
@lhs931
D=M
@rhs932
// These instructions are prescient ... 20130308
A=M
D=A-D
@one934
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done935
0;JMP
(one934)
@1
D=A
// Done with boolean op.
(bool-done935)
@whileend930
D;JEQ
// Run body of whiletop929.
// Set mulsum926.
@mulsum926
D=M
@lhs936
M=D
@mullhs927
D=M
@rhs937
M=D
// Stick LHS result lhs936 in D
@lhs936
D=M
// Stick RHS result rhs937 in A
@rhs937
A=M
D=D+A
@mulsum926
M=D
// Set multctr925.
@multctr925
D=M
@lhs938
M=D
@1
D=A
@rhs939
M=D
// Stick LHS result lhs938 in D
@lhs938
D=M
// Stick RHS result rhs939 in A
@rhs939
A=M
D=D-A
@multctr925
M=D
// Go to top of loop.
@whiletop929
0;JMP
// End of whiletop929.
(whileend930)
@mulsum926
D=M
@rhs909
M=D
// Stick LHS result lhs908 in D
@lhs908
D=M
// Stick RHS result rhs909 in A
@rhs909
A=M
D=D+A
(ifend877)
@rhs858
M=D
// Stick LHS result lhs857 in D
@lhs857
D=M
// Stick RHS result rhs858 in A
@rhs858
A=M
D=D-A
(ifend659)
// Move D register to RAM[0] for final result.
@0
M=D
(ENDOFLINE)
@ENDOFLINE
0;JMP
