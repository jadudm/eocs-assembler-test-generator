// Test 014
// Source: 
//
// (let ((x66 4)) x66)
// 
// Result (in RAM0): 4
// Generated on 20130309 at 19:54.
// -----------------------------------//
@4
D=A
@x66
M=D
@x66
D=M
// Move D register to RAM[0] for final result.
@0
M=D
(ENDOFLINE)
@ENDOFLINE
0;JMP
