// Test 008
// Source: 
//
// (+ 8 (- (let ((x25 0)) x25) 9))
// 
// Result (in RAM0): -1
// Generated on 20130309 at 19:54.
// -----------------------------------//
@8
D=A
@lhs311
M=D
@0
D=A
@x25
M=D
@x25
D=M
@lhs313
M=D
@9
D=A
@rhs314
M=D
// Stick LHS result lhs313 in D
@lhs313
D=M
// Stick RHS result rhs314 in A
@rhs314
A=M
D=D-A
@rhs312
M=D
// Stick LHS result lhs311 in D
@lhs311
D=M
// Stick RHS result rhs312 in A
@rhs312
A=M
D=D+A
// Move D register to RAM[0] for final result.
@0
M=D
(ENDOFLINE)
@ENDOFLINE
0;JMP
