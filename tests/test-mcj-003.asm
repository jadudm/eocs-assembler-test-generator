// Test 003
// Source: 
//
// (let ((sum 0) (x 10)) (begin (while (> x 0) (begin (set! sum (+ x sum)) (set! x (- x 1)))) sum))
// 
// Result (in RAM0): 55
// Generated on 20130309 at 20:3.
// -----------------------------------//
@0
D=A
@sum
M=D
@10
D=A
@x
M=D
// Top of whiletop26
(whiletop26)
// Test; jump to whileend27 if zero.
// Leave LHS in D; ends at bool-done32.
@x
D=M
// Move to lhs28
@lhs28
M=D
// Leave RHS in D.
@0
D=A
@rhs29
M=D
// Subtract LHS from RHS
@lhs28
D=M
@rhs29
// These instructions are prescient ... 20130308
A=M
D=A-D
@one31
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done32
0;JMP
(one31)
@1
D=A
// Done with boolean op.
(bool-done32)
@whileend27
D;JEQ
// Run body of whiletop26.
// Set sum.
@x
D=M
@lhs33
M=D
@sum
D=M
@rhs34
M=D
// Stick LHS result lhs33 in D
@lhs33
D=M
// Stick RHS result rhs34 in A
@rhs34
A=M
D=D+A
@sum
M=D
// Set x.
@x
D=M
@lhs35
M=D
@1
D=A
@rhs36
M=D
// Stick LHS result lhs35 in D
@lhs35
D=M
// Stick RHS result rhs36 in A
@rhs36
A=M
D=D-A
@x
M=D
// Go to top of loop.
@whiletop26
0;JMP
// End of whiletop26.
(whileend27)
@sum
D=M
// Move D register to RAM[0] for final result.
@0
M=D
(ENDOFLINE)
@ENDOFLINE
0;JMP
