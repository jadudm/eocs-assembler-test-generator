// Test 005
// Source: 
//
// (+ (if (> (let ((x15 2)) (- x15 4)) 7) (let ((x16 2)) (let ((x17 6)) (let ((x18 2)) (let ((x19 9)) (let ((x20 7)) (let ((x21 4)) (* 8 1))))))) (+ 7 5)) 7)
// 
// Result (in RAM0): 19
// Generated on 20130309 at 19:54.
// -----------------------------------//
// Do test, leave result in D
// Leave LHS in D; ends at bool-done238.
@2
D=A
@x15
M=D
@x15
D=M
@lhs239
M=D
@4
D=A
@rhs240
M=D
// Stick LHS result lhs239 in D
@lhs239
D=M
// Stick RHS result rhs240 in A
@rhs240
A=M
D=D-A
// Move to lhs234
@lhs234
M=D
// Leave RHS in D.
@7
D=A
@rhs235
M=D
// Subtract LHS from RHS
@lhs234
D=M
@rhs235
// These instructions are prescient ... 20130308
A=M
D=A-D
@one237
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done238
0;JMP
(one237)
@1
D=A
// Done with boolean op.
(bool-done238)
// If the result was zero, we jump to iffalse232
@iffalse232
D;JEQ
@2
D=A
@x16
M=D
@6
D=A
@x17
M=D
@2
D=A
@x18
M=D
@9
D=A
@x19
M=D
@7
D=A
@x20
M=D
@4
D=A
@x21
M=D
@8
D=A
@mullhs243
M=D
@1
D=A
@mulrhs244
M=D
// Set multctr241.
@mulrhs244
D=M
@multctr241
M=D
// Set mulsum242.
@0
D=A
@mulsum242
M=D
// Top of whiletop245
(whiletop245)
// Test; jump to whileend246 if zero.
// Leave LHS in D; ends at bool-done251.
@multctr241
D=M
// Move to lhs247
@lhs247
M=D
// Leave RHS in D.
@0
D=A
@rhs248
M=D
// Subtract LHS from RHS
@lhs247
D=M
@rhs248
// These instructions are prescient ... 20130308
A=M
D=A-D
@one250
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done251
0;JMP
(one250)
@1
D=A
// Done with boolean op.
(bool-done251)
@whileend246
D;JEQ
// Run body of whiletop245.
// Set mulsum242.
@mulsum242
D=M
@lhs252
M=D
@mullhs243
D=M
@rhs253
M=D
// Stick LHS result lhs252 in D
@lhs252
D=M
// Stick RHS result rhs253 in A
@rhs253
A=M
D=D+A
@mulsum242
M=D
// Set multctr241.
@multctr241
D=M
@lhs254
M=D
@1
D=A
@rhs255
M=D
// Stick LHS result lhs254 in D
@lhs254
D=M
// Stick RHS result rhs255 in A
@rhs255
A=M
D=D-A
@multctr241
M=D
// Go to top of loop.
@whiletop245
0;JMP
// End of whiletop245.
(whileend246)
@mulsum242
D=M
// Jump to the ifend233 when done with the true case.
@ifend233
0;JMP
(iffalse232)
@7
D=A
@lhs256
M=D
@5
D=A
@rhs257
M=D
// Stick LHS result lhs256 in D
@lhs256
D=M
// Stick RHS result rhs257 in A
@rhs257
A=M
D=D+A
(ifend233)
@lhs229
M=D
@7
D=A
@rhs230
M=D
// Stick LHS result lhs229 in D
@lhs229
D=M
// Stick RHS result rhs230 in A
@rhs230
A=M
D=D+A
// Move D register to RAM[0] for final result.
@0
M=D
(ENDOFLINE)
@ENDOFLINE
0;JMP
