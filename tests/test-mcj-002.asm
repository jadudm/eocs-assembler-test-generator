// Test 002
// Source: 
//
// (* 2 4)
// 
// Result (in RAM0): 8
// Generated on 20130309 at 20:3.
// -----------------------------------//
@2
D=A
@mullhs13
M=D
@4
D=A
@mulrhs14
M=D
// Set multctr11.
@mulrhs14
D=M
@multctr11
M=D
// Set mulsum12.
@0
D=A
@mulsum12
M=D
// Top of whiletop15
(whiletop15)
// Test; jump to whileend16 if zero.
// Leave LHS in D; ends at bool-done21.
@multctr11
D=M
// Move to lhs17
@lhs17
M=D
// Leave RHS in D.
@0
D=A
@rhs18
M=D
// Subtract LHS from RHS
@lhs17
D=M
@rhs18
// These instructions are prescient ... 20130308
A=M
D=A-D
@one20
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done21
0;JMP
(one20)
@1
D=A
// Done with boolean op.
(bool-done21)
@whileend16
D;JEQ
// Run body of whiletop15.
// Set mulsum12.
@mulsum12
D=M
@lhs22
M=D
@mullhs13
D=M
@rhs23
M=D
// Stick LHS result lhs22 in D
@lhs22
D=M
// Stick RHS result rhs23 in A
@rhs23
A=M
D=D+A
@mulsum12
M=D
// Set multctr11.
@multctr11
D=M
@lhs24
M=D
@1
D=A
@rhs25
M=D
// Stick LHS result lhs24 in D
@lhs24
D=M
// Stick RHS result rhs25 in A
@rhs25
A=M
D=D-A
@multctr11
M=D
// Go to top of loop.
@whiletop15
0;JMP
// End of whiletop15.
(whileend16)
@mulsum12
D=M
// Move D register to RAM[0] for final result.
@0
M=D
(ENDOFLINE)
@ENDOFLINE
0;JMP
