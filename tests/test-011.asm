// Test 011
// Source: 
//
// (if (< 6 (- (let ((x37 5)) (+ x37 5)) (let ((x38 8)) (+ (if (> (* 8 x38) x38) (let ((x39 9)) (let ((x40 8)) (let ((x41 5)) x41))) (+ 9 8)) 2)))) (if (< (+ (+ (let ((x42 5)) (let ((x43 2)) 5)) (* 6 8)) 1) (let ((x44 5)) x44)) (if (> 6 (let ((x45 9)) x45)) (let ((x46 1)) (+ x46 (if (> 7 4) 4 7))) 5) (* (let ((x47 4)) x47) (+ (+ 3 7) (if (> 4 7) 1 7)))) (let ((x48 1)) (- (* (* (* 1 9) (- 0 x48)) (let ((x49 1)) (let ((x50 9)) (let ((x51 5)) 3)))) (let ((x52 0)) (let ((x53 7)) x48)))))
// 
// Result (in RAM0): -28
// Generated on 20130309 at 19:54.
// -----------------------------------//
// Do test, leave result in D
// Leave LHS in D; ends at bool-done504.
@6
D=A
// Move to lhs500
@lhs500
M=D
// Leave RHS in D.
@5
D=A
@x37
M=D
@x37
D=M
@lhs507
M=D
@5
D=A
@rhs508
M=D
// Stick LHS result lhs507 in D
@lhs507
D=M
// Stick RHS result rhs508 in A
@rhs508
A=M
D=D+A
@lhs505
M=D
@8
D=A
@x38
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done518.
@8
D=A
@mullhs521
M=D
@x38
D=M
@mulrhs522
M=D
// Set multctr519.
@mulrhs522
D=M
@multctr519
M=D
// Set mulsum520.
@0
D=A
@mulsum520
M=D
// Top of whiletop523
(whiletop523)
// Test; jump to whileend524 if zero.
// Leave LHS in D; ends at bool-done529.
@multctr519
D=M
// Move to lhs525
@lhs525
M=D
// Leave RHS in D.
@0
D=A
@rhs526
M=D
// Subtract LHS from RHS
@lhs525
D=M
@rhs526
// These instructions are prescient ... 20130308
A=M
D=A-D
@one528
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done529
0;JMP
(one528)
@1
D=A
// Done with boolean op.
(bool-done529)
@whileend524
D;JEQ
// Run body of whiletop523.
// Set mulsum520.
@mulsum520
D=M
@lhs530
M=D
@mullhs521
D=M
@rhs531
M=D
// Stick LHS result lhs530 in D
@lhs530
D=M
// Stick RHS result rhs531 in A
@rhs531
A=M
D=D+A
@mulsum520
M=D
// Set multctr519.
@multctr519
D=M
@lhs532
M=D
@1
D=A
@rhs533
M=D
// Stick LHS result lhs532 in D
@lhs532
D=M
// Stick RHS result rhs533 in A
@rhs533
A=M
D=D-A
@multctr519
M=D
// Go to top of loop.
@whiletop523
0;JMP
// End of whiletop523.
(whileend524)
@mulsum520
D=M
// Move to lhs514
@lhs514
M=D
// Leave RHS in D.
@x38
D=M
@rhs515
M=D
// Subtract LHS from RHS
@lhs514
D=M
@rhs515
// These instructions are prescient ... 20130308
A=M
D=A-D
@one517
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done518
0;JMP
(one517)
@1
D=A
// Done with boolean op.
(bool-done518)
// If the result was zero, we jump to iffalse512
@iffalse512
D;JEQ
@9
D=A
@x39
M=D
@8
D=A
@x40
M=D
@5
D=A
@x41
M=D
@x41
D=M
// Jump to the ifend513 when done with the true case.
@ifend513
0;JMP
(iffalse512)
@9
D=A
@lhs534
M=D
@8
D=A
@rhs535
M=D
// Stick LHS result lhs534 in D
@lhs534
D=M
// Stick RHS result rhs535 in A
@rhs535
A=M
D=D+A
(ifend513)
@lhs509
M=D
@2
D=A
@rhs510
M=D
// Stick LHS result lhs509 in D
@lhs509
D=M
// Stick RHS result rhs510 in A
@rhs510
A=M
D=D+A
@rhs506
M=D
// Stick LHS result lhs505 in D
@lhs505
D=M
// Stick RHS result rhs506 in A
@rhs506
A=M
D=D-A
@rhs501
M=D
// Subtract LHS from RHS
@lhs500
D=M
@rhs501
// These instructions are prescient ... 20130308
A=M
D=A-D
@one503
// Jump and leave 1 in D if true.
D;JGT
@0
D=A
@bool-done504
0;JMP
(one503)
@1
D=A
// Done with boolean op.
(bool-done504)
// If the result was zero, we jump to iffalse498
@iffalse498
D;JEQ
// Do test, leave result in D
// Leave LHS in D; ends at bool-done543.
@5
D=A
@x42
M=D
@2
D=A
@x43
M=D
@5
D=A
@lhs546
M=D
@6
D=A
@mullhs550
M=D
@8
D=A
@mulrhs551
M=D
// Set multctr548.
@mulrhs551
D=M
@multctr548
M=D
// Set mulsum549.
@0
D=A
@mulsum549
M=D
// Top of whiletop552
(whiletop552)
// Test; jump to whileend553 if zero.
// Leave LHS in D; ends at bool-done558.
@multctr548
D=M
// Move to lhs554
@lhs554
M=D
// Leave RHS in D.
@0
D=A
@rhs555
M=D
// Subtract LHS from RHS
@lhs554
D=M
@rhs555
// These instructions are prescient ... 20130308
A=M
D=A-D
@one557
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done558
0;JMP
(one557)
@1
D=A
// Done with boolean op.
(bool-done558)
@whileend553
D;JEQ
// Run body of whiletop552.
// Set mulsum549.
@mulsum549
D=M
@lhs559
M=D
@mullhs550
D=M
@rhs560
M=D
// Stick LHS result lhs559 in D
@lhs559
D=M
// Stick RHS result rhs560 in A
@rhs560
A=M
D=D+A
@mulsum549
M=D
// Set multctr548.
@multctr548
D=M
@lhs561
M=D
@1
D=A
@rhs562
M=D
// Stick LHS result lhs561 in D
@lhs561
D=M
// Stick RHS result rhs562 in A
@rhs562
A=M
D=D-A
@multctr548
M=D
// Go to top of loop.
@whiletop552
0;JMP
// End of whiletop552.
(whileend553)
@mulsum549
D=M
@rhs547
M=D
// Stick LHS result lhs546 in D
@lhs546
D=M
// Stick RHS result rhs547 in A
@rhs547
A=M
D=D+A
@lhs544
M=D
@1
D=A
@rhs545
M=D
// Stick LHS result lhs544 in D
@lhs544
D=M
// Stick RHS result rhs545 in A
@rhs545
A=M
D=D+A
// Move to lhs539
@lhs539
M=D
// Leave RHS in D.
@5
D=A
@x44
M=D
@x44
D=M
@rhs540
M=D
// Subtract LHS from RHS
@lhs539
D=M
@rhs540
// These instructions are prescient ... 20130308
A=M
D=A-D
@one542
// Jump and leave 1 in D if true.
D;JGT
@0
D=A
@bool-done543
0;JMP
(one542)
@1
D=A
// Done with boolean op.
(bool-done543)
// If the result was zero, we jump to iffalse537
@iffalse537
D;JEQ
// Do test, leave result in D
// Leave LHS in D; ends at bool-done570.
@6
D=A
// Move to lhs566
@lhs566
M=D
// Leave RHS in D.
@9
D=A
@x45
M=D
@x45
D=M
@rhs567
M=D
// Subtract LHS from RHS
@lhs566
D=M
@rhs567
// These instructions are prescient ... 20130308
A=M
D=A-D
@one569
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done570
0;JMP
(one569)
@1
D=A
// Done with boolean op.
(bool-done570)
// If the result was zero, we jump to iffalse564
@iffalse564
D;JEQ
@1
D=A
@x46
M=D
@x46
D=M
@lhs571
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done580.
@7
D=A
// Move to lhs576
@lhs576
M=D
// Leave RHS in D.
@4
D=A
@rhs577
M=D
// Subtract LHS from RHS
@lhs576
D=M
@rhs577
// These instructions are prescient ... 20130308
A=M
D=A-D
@one579
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done580
0;JMP
(one579)
@1
D=A
// Done with boolean op.
(bool-done580)
// If the result was zero, we jump to iffalse574
@iffalse574
D;JEQ
@4
D=A
// Jump to the ifend575 when done with the true case.
@ifend575
0;JMP
(iffalse574)
@7
D=A
(ifend575)
@rhs572
M=D
// Stick LHS result lhs571 in D
@lhs571
D=M
// Stick RHS result rhs572 in A
@rhs572
A=M
D=D+A
// Jump to the ifend565 when done with the true case.
@ifend565
0;JMP
(iffalse564)
@5
D=A
(ifend565)
// Jump to the ifend538 when done with the true case.
@ifend538
0;JMP
(iffalse537)
@4
D=A
@x47
M=D
@x47
D=M
@mullhs583
M=D
@3
D=A
@lhs587
M=D
@7
D=A
@rhs588
M=D
// Stick LHS result lhs587 in D
@lhs587
D=M
// Stick RHS result rhs588 in A
@rhs588
A=M
D=D+A
@lhs585
M=D
// Do test, leave result in D
// Leave LHS in D; ends at bool-done596.
@4
D=A
// Move to lhs592
@lhs592
M=D
// Leave RHS in D.
@7
D=A
@rhs593
M=D
// Subtract LHS from RHS
@lhs592
D=M
@rhs593
// These instructions are prescient ... 20130308
A=M
D=A-D
@one595
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done596
0;JMP
(one595)
@1
D=A
// Done with boolean op.
(bool-done596)
// If the result was zero, we jump to iffalse590
@iffalse590
D;JEQ
@1
D=A
// Jump to the ifend591 when done with the true case.
@ifend591
0;JMP
(iffalse590)
@7
D=A
(ifend591)
@rhs586
M=D
// Stick LHS result lhs585 in D
@lhs585
D=M
// Stick RHS result rhs586 in A
@rhs586
A=M
D=D+A
@mulrhs584
M=D
// Set multctr581.
@mulrhs584
D=M
@multctr581
M=D
// Set mulsum582.
@0
D=A
@mulsum582
M=D
// Top of whiletop597
(whiletop597)
// Test; jump to whileend598 if zero.
// Leave LHS in D; ends at bool-done603.
@multctr581
D=M
// Move to lhs599
@lhs599
M=D
// Leave RHS in D.
@0
D=A
@rhs600
M=D
// Subtract LHS from RHS
@lhs599
D=M
@rhs600
// These instructions are prescient ... 20130308
A=M
D=A-D
@one602
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done603
0;JMP
(one602)
@1
D=A
// Done with boolean op.
(bool-done603)
@whileend598
D;JEQ
// Run body of whiletop597.
// Set mulsum582.
@mulsum582
D=M
@lhs604
M=D
@mullhs583
D=M
@rhs605
M=D
// Stick LHS result lhs604 in D
@lhs604
D=M
// Stick RHS result rhs605 in A
@rhs605
A=M
D=D+A
@mulsum582
M=D
// Set multctr581.
@multctr581
D=M
@lhs606
M=D
@1
D=A
@rhs607
M=D
// Stick LHS result lhs606 in D
@lhs606
D=M
// Stick RHS result rhs607 in A
@rhs607
A=M
D=D-A
@multctr581
M=D
// Go to top of loop.
@whiletop597
0;JMP
// End of whiletop597.
(whileend598)
@mulsum582
D=M
(ifend538)
// Jump to the ifend499 when done with the true case.
@ifend499
0;JMP
(iffalse498)
@1
D=A
@x48
M=D
@1
D=A
@mullhs620
M=D
@9
D=A
@mulrhs621
M=D
// Set multctr618.
@mulrhs621
D=M
@multctr618
M=D
// Set mulsum619.
@0
D=A
@mulsum619
M=D
// Top of whiletop622
(whiletop622)
// Test; jump to whileend623 if zero.
// Leave LHS in D; ends at bool-done628.
@multctr618
D=M
// Move to lhs624
@lhs624
M=D
// Leave RHS in D.
@0
D=A
@rhs625
M=D
// Subtract LHS from RHS
@lhs624
D=M
@rhs625
// These instructions are prescient ... 20130308
A=M
D=A-D
@one627
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done628
0;JMP
(one627)
@1
D=A
// Done with boolean op.
(bool-done628)
@whileend623
D;JEQ
// Run body of whiletop622.
// Set mulsum619.
@mulsum619
D=M
@lhs629
M=D
@mullhs620
D=M
@rhs630
M=D
// Stick LHS result lhs629 in D
@lhs629
D=M
// Stick RHS result rhs630 in A
@rhs630
A=M
D=D+A
@mulsum619
M=D
// Set multctr618.
@multctr618
D=M
@lhs631
M=D
@1
D=A
@rhs632
M=D
// Stick LHS result lhs631 in D
@lhs631
D=M
// Stick RHS result rhs632 in A
@rhs632
A=M
D=D-A
@multctr618
M=D
// Go to top of loop.
@whiletop622
0;JMP
// End of whiletop622.
(whileend623)
@mulsum619
D=M
@mullhs616
M=D
@0
D=A
@lhs633
M=D
@x48
D=M
@rhs634
M=D
// Stick LHS result lhs633 in D
@lhs633
D=M
// Stick RHS result rhs634 in A
@rhs634
A=M
D=D-A
@mulrhs617
M=D
// Set multctr614.
@mulrhs617
D=M
@multctr614
M=D
// Set mulsum615.
@0
D=A
@mulsum615
M=D
// Top of whiletop635
(whiletop635)
// Test; jump to whileend636 if zero.
// Leave LHS in D; ends at bool-done641.
@multctr614
D=M
// Move to lhs637
@lhs637
M=D
// Leave RHS in D.
@0
D=A
@rhs638
M=D
// Subtract LHS from RHS
@lhs637
D=M
@rhs638
// These instructions are prescient ... 20130308
A=M
D=A-D
@one640
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done641
0;JMP
(one640)
@1
D=A
// Done with boolean op.
(bool-done641)
@whileend636
D;JEQ
// Run body of whiletop635.
// Set mulsum615.
@mulsum615
D=M
@lhs642
M=D
@mullhs616
D=M
@rhs643
M=D
// Stick LHS result lhs642 in D
@lhs642
D=M
// Stick RHS result rhs643 in A
@rhs643
A=M
D=D+A
@mulsum615
M=D
// Set multctr614.
@multctr614
D=M
@lhs644
M=D
@1
D=A
@rhs645
M=D
// Stick LHS result lhs644 in D
@lhs644
D=M
// Stick RHS result rhs645 in A
@rhs645
A=M
D=D-A
@multctr614
M=D
// Go to top of loop.
@whiletop635
0;JMP
// End of whiletop635.
(whileend636)
@mulsum615
D=M
@mullhs612
M=D
@1
D=A
@x49
M=D
@9
D=A
@x50
M=D
@5
D=A
@x51
M=D
@3
D=A
@mulrhs613
M=D
// Set multctr610.
@mulrhs613
D=M
@multctr610
M=D
// Set mulsum611.
@0
D=A
@mulsum611
M=D
// Top of whiletop646
(whiletop646)
// Test; jump to whileend647 if zero.
// Leave LHS in D; ends at bool-done652.
@multctr610
D=M
// Move to lhs648
@lhs648
M=D
// Leave RHS in D.
@0
D=A
@rhs649
M=D
// Subtract LHS from RHS
@lhs648
D=M
@rhs649
// These instructions are prescient ... 20130308
A=M
D=A-D
@one651
// Jump and leave 1 in D if true.
D;JLT
@0
D=A
@bool-done652
0;JMP
(one651)
@1
D=A
// Done with boolean op.
(bool-done652)
@whileend647
D;JEQ
// Run body of whiletop646.
// Set mulsum611.
@mulsum611
D=M
@lhs653
M=D
@mullhs612
D=M
@rhs654
M=D
// Stick LHS result lhs653 in D
@lhs653
D=M
// Stick RHS result rhs654 in A
@rhs654
A=M
D=D+A
@mulsum611
M=D
// Set multctr610.
@multctr610
D=M
@lhs655
M=D
@1
D=A
@rhs656
M=D
// Stick LHS result lhs655 in D
@lhs655
D=M
// Stick RHS result rhs656 in A
@rhs656
A=M
D=D-A
@multctr610
M=D
// Go to top of loop.
@whiletop646
0;JMP
// End of whiletop646.
(whileend647)
@mulsum611
D=M
@lhs608
M=D
@0
D=A
@x52
M=D
@7
D=A
@x53
M=D
@x48
D=M
@rhs609
M=D
// Stick LHS result lhs608 in D
@lhs608
D=M
// Stick RHS result rhs609 in A
@rhs609
A=M
D=D-A
(ifend499)
// Move D register to RAM[0] for final result.
@0
M=D
(ENDOFLINE)
@ENDOFLINE
0;JMP
