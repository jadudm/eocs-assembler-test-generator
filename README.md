# EoCS Test Generator

You've written your assembler, but does it work?

This small script is essentially a toy compiler for S-expression-based programs that contain:

* let
* while
* if
* variable references
* numbers

They compile down to an inefficient Hack assembly language, which can be further converted to Hack microcode. Each test file contains a commented header that includes the result that should end up in location RAM[0].

A properly functioning assembler will, after conversion, produce a microcode file that:

1. Can be loaded into the EoCS CPU emulator,
1. Executed,
1. Will leave the correct result in RAM[0], and
1. Fall into an infinite loop.

These files are located in the **tests/** subdirectory.

## all-possible-instructions.asm

This file does not execute correctly on the CPU emulator. Instead, it is simply a list of every single possible assembly language instruction in the Hack CPU. Your assembler should be able to convert this entire file into microcode, correctly.